# 
# Translators:
# bf5man <bf5man@gmail.com>, 2016
# Clement Krys <krys.clement@gmail.com>, 2016
# Matthieu S <utybo@users.noreply.github.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-06-11 01:07+0000\n"
"Last-Translator: Matthieu S <utybo@users.noreply.github.com>\n"
"Language-Team: French (France) (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/fr_FR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr_FR\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: chatroom.html:18
msgid "Chat Room"
msgstr "Salle de Chat"

#: chatroom.html:27
msgid "Chat with fellow users"
msgstr "Chattez avec d'autres utilisateurs"

#: chatroom.html:28
msgid "This IRC client is pre-configured so you can jump into our"
msgstr "Ce client IRC est pré-configuré. Vous pouvez donc vous connecter à notre canal"

#: chatroom.html:29
msgid "channel on"
msgstr "sur"

#: chatroom.html:30
msgid ""
"Most of the Ubuntu MATE team are on here but they have real lives too. If "
"you have a question, feel free to ask."
msgstr "Une grande partie de l'équipe Ubuntu MATE y est connectée mais prennez en compte qu'ils ont aussi une vie. Si vous avez une question, n'hésitez pas à la poser."

#: chatroom.html:32
msgid ""
"However, it may take a while for someone to reply. Just be patient and don't"
" disconnect right away."
msgstr "Cependant, il est possible que vous deviez attendre un certain temps avant que quelqu'un réponde. Soyez patient(e), ne vous déconnectez pas tout de suite."

#: chatroom.html:36
msgid ""
"It appears you are not connected to the Internet. Please check your "
"connection to access this content."
msgstr "Il apparaît que vous n'êtes pas connecté(e) à Internet. Veuillez vérifier votre connection afin d'accéder à ce contenu."

#: chatroom.html:37
msgid "Sorry, Welcome was unable to establish a connection."
msgstr "Désolé, Bienvenue n'a pas été en mesure d'établir une connexion."

#: chatroom.html:38
msgid "Retry"
msgstr "Réessayer"

#: chatroom.html:41
msgid "Join the IRC in Hexchat"
msgstr "Se connecter à IRC via Hexchat."
