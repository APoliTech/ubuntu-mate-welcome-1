# 
# Translators:
# Mubarak Qahtani <abu-q76@hotmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-06-19 04:27+0000\n"
"Last-Translator: Mubarak Qahtani <abu-q76@hotmail.com>\n"
"Language-Team: Arabic (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/ar/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ar\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: donate.html:18
msgid "Donate"
msgstr "تبرع"

#: donate.html:27
msgid "About Donations"
msgstr "حول التبرع"

#: donate.html:28
msgid "Our Supporters"
msgstr "المساهمون"

#: donate.html:31
msgid ""
"It appears you are not connected to the Internet. Please check your "
"connection to access this content."
msgstr "من الواضح انك لست متصلًا بالانترنت. رجاءًا تأكد من اتصالك بالانترنت لكي ترى هذا المحتوى"

#: donate.html:32
msgid "Sorry, Welcome was unable to establish a connection."
msgstr "عفوًا، برنامج الترحيب ليس قادر على انشاء اتصال"

#: donate.html:33
msgid "Retry"
msgstr "إعادة محاولة"

#: donate.html:42
msgid ""
"Ubuntu MATE is funded by our community. Your donations help pay towards:"
msgstr "\"أبونتو ماتيه\" وُجِدَتْ من هذا المجتمع. تبرعاتكم تساعد في دفعنا إلى الأمام."

#: donate.html:44
msgid "Web hosting and bandwidth costs."
msgstr "تكاليف اﻹستضافة وكمية استهلاك الأنترنت"

#: donate.html:45
msgid "Supporting Open Source projects that Ubuntu MATE depends upon."
msgstr "دعم المشاريع مفتوحة المصدر والتي تعتمد عليها \"أبونتو ماتيه\""

#: donate.html:46
msgid ""
"Eventually pay for full time developers of Ubuntu MATE and MATE Desktop."
msgstr ""

#: donate.html:48
msgid "Patreon"
msgstr ""

#: donate.html:48
msgid ""
"is a unique way to fund an Open Source project. A regular monthly income "
"ensures sustainability for the Ubuntu MATE project. Patrons will be rewarded"
" with exclusive project news, updates and invited to participate in video "
"conferences where you can talk to the developers directly."
msgstr ""

#: donate.html:53
msgid "If you would prefer to use"
msgstr ""

#: donate.html:53
msgid ""
"then we have options to donate monthly or make a one off donation. Finally, "
"we also accept donations via"
msgstr ""

#: donate.html:55
msgid "Bitcoin"
msgstr "العملة الإليكترونية"

#: donate.html60, 73
msgid "Become a Patron"
msgstr ""

#: donate.html:63
msgid "Donate with PayPal"
msgstr ""

#: donate.html:66
msgid "Donate with Bitcoin"
msgstr ""

#: donate.html:72
msgid "Commercial sponsorship"
msgstr ""

#: donate.html:72
msgid "is also available. Click the"
msgstr ""

#: donate.html:73
msgid "button above to find out more."
msgstr ""

#: donate.html:81
msgid "Thank you!"
msgstr "شكرًا لك!"

#: donate.html:82
msgid ""
"Every month, we compile a list of our donations and detail how they are "
"spent."
msgstr ""

#: donate.html:83
msgid "This information is published monthly to the"
msgstr ""

#: donate.html:83
msgid "Ubuntu MATE Blog"
msgstr "\"أبونتو ماتيه\" مدونة"

#: donate.html:84
msgid "which you can take a look below."
msgstr ""

#: donate.html:90
msgid "Year"
msgstr "سنة"

#: donate.html:91
msgid "Month"
msgstr "شهر"
