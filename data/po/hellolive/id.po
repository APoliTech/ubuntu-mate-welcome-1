# 
# Translators:
# Guz Firdaus <guzfirdaus@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-06-06 16:27+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: Indonesian (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/id/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: id\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: hellolive.html15, 29
msgid "Welcome"
msgstr "Selamat Datang"

#: hellolive.html:25
msgid "Hello."
msgstr "Hai."

#: hellolive.html:26
msgid "Thank you for downloading Ubuntu MATE."
msgstr ""

#: hellolive.html:28
msgid "The"
msgstr "The"

#: hellolive.html:29
msgid ""
"application is your companion for getting started. Once installed, the "
"Software Boutique is available to install a selection of featured "
"applications to help you get the most out of your computing experience."
msgstr ""

#: hellolive.html:33
msgid "We hope you enjoy Ubuntu MATE."
msgstr "Kami harap Anda menikmati Ubuntu MATE."

#: hellolive.html:35
msgid "Continue"
msgstr "Lanjutkan"
