# 
# Translators:
# peter, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-06-06 16:10+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: Slovak (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/sk/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sk\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: index.html:14
msgid "Main Menu"
msgstr "Hlavné menu"

#: index.html:17
msgid ""
"Ensures Welcome's documentation, translations and software picks are always "
"up-to-date."
msgstr ""

#: index.html:18
msgid "Subscribe to Welcome updates"
msgstr "Prihlásiť sa k odberu noviniek"

#: index.html:22
msgid "Welcome will automatically restart in a few moments..."
msgstr "Vitajte sa automaticky reštartuje..."

#: index.html:37
msgid "Choose an option to discover your new operating system."
msgstr "Vyberte si možnosti k objaveniu Vášho nového systému"

#: index.html:45
msgid "Introduction"
msgstr "Predstavenie"

#: index.html:46
msgid "Features"
msgstr "Funkcie"

#: index.html:47
msgid "Getting Started"
msgstr "Začíname"

#: index.html:48
msgid "Installation Help"
msgstr "Pomocník inštalácie"

#: index.html:53
msgid "Get Involved"
msgstr "Zapojiť sa"

#: index.html:54
msgid "Shop"
msgstr "Obchod"

#: index.html:55
msgid "Donate"
msgstr "Prispieť"

#: index.html:63
msgid "Community"
msgstr "Komunita"

#: index.html:64
msgid "Chat Room"
msgstr "Četovacia miestnosť"

#: index.html:65
msgid "Software"
msgstr "Softvér"

#: index.html:66
msgid "Install Now"
msgstr "Inštalovať"

#: index.html:73
msgid "Raspberry Pi Information"
msgstr "Raspberry Pi informácie"

#: index.html:86
msgid "Open Welcome when I log on."
msgstr "Otvoriť Vitajte pri prihlásení"
